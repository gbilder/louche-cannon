{{- $isodate := (.Name | replaceRE `^([0-9]{4}-[0-9]{2}-[0-9]{2}).*` `$1`) -}}
{{- $posttitle := (.Name | replaceRE `^[0-9]{4}-[0-9]{2}-[0-9]{2}-` "" | replaceRE `-` " " | title) -}}
---
title: "{{ $posttitle }}"
date: "{{ $isodate }}"
lastmod: "{{ $isodate }}"
draft: true
description: "{{ $posttitle }}"
# Summary: "Uncomment, and fill this in if you don't want the first lines of body text used"
tags: []
ShowToc: true
ShowBreadCrumbs: false

# cover:
#     image: "covers/a-cover-image.jpeg" # partial path/url
#     alt: "some alt text" # alt text
#     caption: "<banner-image-caption>" # display caption under cover
#     relative: false # when using page bundles set this to true
#     hidden: false # only hide on current single page
params:
  ShowReadingTime: true
  syndicate: false
  #doi: ""
  authors:
    - name: "Geoffrey Bilder"
      orcid: "https://orcid.org/0000-0003-1315-5960"

---