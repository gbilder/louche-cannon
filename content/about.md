---
title: About
author: gbilder
type: page
date: 2024-06-24
draft: false
showtoc: false
---

## Geoffrey Bilder

Louche Cannon. Identifier dweeb. Open scholarly infrastructure pontificator. Grew up in 🇵🇷 . Fled 🇺🇸 & 🇬🇧. Happy resident of 🇫🇷 in 🇪🇺. Dog & cat person. But mostly dogs.

If you want to get hold of me and you don't already have contact info, then at the moment (circa 2025), your best bet is to contact me on:

Signal: [`gbilder`](https://signal.me/#eu/f_NPJxbTDvj9y-PlZWjxY9XXVApe-PCDGV15QDHbME7FDx9YY4VB57xT5qM7PGW1)

Mastodon: [`@gbilder@digipres.club`](https://digipres.club/@gbilder)

Bluesky: [`@gbilder.com`](https://bsky.app/profile/gbilder.com)

Or email at:

`gbilder`

at

`gbilder.com`

As far as I know, there are two Geoffrey Bilder's in the world. In case you are trying to figure out which one I am- I'm the one who:

- Grew up in Puerto Rico.
- Attended [Baldwin](https://www.baldwin-school.org), [Bausfield](https://www.bousfieldprimaryschool.co.uk), [Bowdoin](https://bowdoin.edu), [Brown](https://brown.edu).
- Worked at [Brown University Scholarly Technology Group](https://library.brown.edu/create/cds/), [Monitor Group](https://en.wikipedia.org/wiki/Monitor_Deloitte), [Dynamic Diagrams](https://www.dynamicdiagrams.com/index.html), [Ingenta](https://www.ingenta.com), Scholarly Information Strategies, and  [Crossref](https://crossref.org).
- Lives in [Nîmes](https://fr.wikipedia.org/wiki/Nîmes) with my partner, Penny (aka Galoot).

None of the above is the answer to any of my "secret questions."

I also have a [somewhat neglected blog](/posts)

But that doesn't mean I don't blog or write- it just means I haven't done it on my own blog.

## Credits

[Front page image courtesy of WikiData](https://www.wikidata.org/wiki/Q45314579)

The site is built with [Hugo](https://gohugo.io).

It also uses the following:
- [PaperMod](https://github.com/adityatelange/hugo-PaperMod)
- [hugo-admonitions](https://github.com/KKKZOZ/hugo-admonitions/tree/main)
- [hugo-shortcode-gallery](https://github.com/mfg92/hugo-shortcode-gallery/tree/master)
