---
title: "Noise"
date: 2025-01-11
type: page
draft: false
showtoc: false
---

## Noise

A very long time ago I played with [trackers](https://en.wikipedia.org/wiki/Music_tracker).

I can only find two surviving... I hesitate to call them "songs."

But whatever they are, here they are.

I created both of these at a tribute to the staff of an overstretched help desk who were being treated like crap by a bunch of entitled and ignorant jerks.

### You have reached the helpdesk

{{<audio src="/music/helpdesk.mp3" caption="You have reached the helpdesk">}}


### The following is a message

{{<audio src="/music/radnet.mp3" caption="The following is a message">}}







