---
title: "Instagram"
date: 2025-01-11
type: page
draft: false
showtoc: false
author: ["Geoffrey Bilder"]
---

## What is this?

My old Instagram pictures.

{{< gallery match="images/*" sortOrder="desc" rowHeight="150" margins="5" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview=true loadJQuery=true >}}





