---
title: "POSI Posse Self-Assessment Dashboard"
date: 2024-10-27
type: page
draft: false
showtoc: false
---


{{< posi-summary >}}

## What is this?

The current POSI site lists the current POSI adoptors and links to their self-assessments on the "[POSI Posse](https://openscholarlyinfrastructure.org/posse/)" page.

FTR- I created the first version of this list. But I can't edit it any more.

And, over time, the list's formatting has become wildly inconsistent, making it difficult to tell which organizations are taking their POSI self-assessments seriously.

This is an attempt to fix that.

The list of members and the dates of their POSI endorsements is refreshed automaticaly once a day. The last refresh date is noted above the list. The calculation of the number of days since their last update is calculated every time the page is refreshed.

## How did you do it?

I periodically scrape the POSI Posse list and convert it into this.

This is one of the rare times an LLM is really handy. The existing POSI Posse list is formatted so inconsistently that writing an actual parser would have meant creating a special rule for practically every line in the list. Doing this would have been brittle and stupid.

So, instead, it is easier to throw the list at an LLM along with a JSON schema and ask the LLM to extract the relevant information and return it in JSON that conforms to the provided schema.

Easiest of all would be for somebody to fix the Posse list on the POSI site and make it more useful.



