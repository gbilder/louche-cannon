---
title: Didcot
author: gbilder
type: post
date: 2004-01-14T19:24:22+00:00
url: /2004/01/didcot/
categories:
  - Photos
  - Travel
---

I have to pass through Didcot when I take the train to London or Bath. Every time I see the power station, it makes me want to eat glass.


You think I exaggerate? See the photos&#8230;

January 14, 2004


{{< figure src="assets/img/40D80013.jpg" title="Didcot 1" >}}
{{< figure src="assets/img/40D80016.jpg" title="Didcot 2" >}}


