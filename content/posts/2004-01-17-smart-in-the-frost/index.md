---
title: Smart in the frost
author: gbilder
type: post
date: 2004-01-17T23:13:25+00:00
url: /2004/01/smart-in-the-frost/
categories:
  - Personal
  - Photos
---

Frost is unusual in Oxford. Clearing the windshield of our Smart Car is a doodle. Sure beats having to clear snow off of a regular sized car. I do not miss New England weather.

{{< figure src="assets/img/smart_frost.jpg" title="Smartcar in the frost" >}}

