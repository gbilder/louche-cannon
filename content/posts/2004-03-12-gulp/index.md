---
title: Gulp…
author: gbilder
type: post
date: 2004-03-12T23:34:53+00:00
url: /2004/03/gulp/
categories:
  - Photos
  - Travel
---

Taken from the stage about 30 minutes before my talk I wasn&#8217;t quite expecting such a big venue.

Seconds before I start, the moderator leans over to me and says “You know- this is the stage that Ronald Reagan spoke from just before he was shot”.

I think we was trying to calm my nerves.

{{< figure src="assets/img/42BL0051.jpg" title="DC Hilton" >}}

