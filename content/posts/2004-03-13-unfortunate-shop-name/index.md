---
title: Unfortunate shop name…
author: gbilder
type: post
date: 2004-03-13T23:16:34+00:00
url: /2004/03/unfortunate-shop-name/
categories:
  - Personal
  - Photos
---

Say it fast&#8230;

It still makes me giggle.

{{< figure src="assets/img/unfortunate_sign1.jpg" title="Aerosoles" >}}
