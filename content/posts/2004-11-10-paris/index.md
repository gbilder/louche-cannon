---
title: Paris
author: gbilder
type: post
date: 2004-11-10T23:05:51+00:00
url: /2004/11/paris/
categories:
  - Photos
  - Travel
---

Quick trip day-trip on the Eurostar. Very peculiar taking the train from Oxford to Paris.

I had to shoot a few horrible pix with my phone in order to convince myself it was all real.

[See Photos]

{{< figure src="assets/img/paris1.jpg">}}
{{< figure src="assets/img/paris2.jpg">}}
{{< figure src="assets/img/paris3.jpg">}}
{{< figure src="assets/img/paris4.jpg">}}


