---
title: Winter walk to Leon
author: gbilder
type: post
date: 2004-12-21T23:26:37+00:00
url: /2004/12/winter-walk-to-leon/
categories:
  - Photos
  - Travel
---

The land of Asterix and Obelix.

{{< figure src="assets/img/leon_walk1-2.jpg">}}


Though presumably the sign is relatively recent.

{{< figure src="assets/img/leon_walk2-3.jpg">}}
