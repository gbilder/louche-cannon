---
title: First day back to work 2005
author: gbilder
type: post
date: 2005-01-05T23:00:21+00:00
url: /2005/01/first-day-back-to-work-2005/
categories:
  - Photos
---

At least it was a pretty walk in.

{{< figure src="assets/img/silver_road_sunrise.jpg" title="Silver Road sunrise">}}
