---
title: Oxford Snow
author: gbilder
type: post
date: 2005-02-23T11:22:20+00:00
url: /2005/02/oxford-snow/
categories:
  - Dog
  - Personal
  - Photos
---

Snowed in Oxford last week. A strange week for so many reasons&#8230;

{{< figure src="assets/img/IMG_2387_1.jpg">}}



The back ~~yard~~ garden.

{{< figure src="assets/img/garden_snow.jpg">}}


And Chickpea.

{{< figure src="assets/img/cp_snow1.jpg">}}


{{< figure src="assets/img/cp_snow2.jpg">}}

