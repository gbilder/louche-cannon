---
title: Cafe Orient
author: gbilder
type: post
date: 2005-03-05T18:25:03+00:00
url: /2005/03/cafe-orient/
categories:
  - Food
  - Photos
  - Travel
---


> [!WARNING]
Cafe Orient closed around 2006

---

If you live in Oxford and have not been to the Cafe Orient, drop what you are doing and go straight there now.

If you are visiting Oxford, ignore all the restaurant advice that you find in guidebooks and go to the Cafe Orient instead.

{{< figure src="assets/img/cafe_orient1.jpg" title="Cafe Orient">}}


Cafe Orient
77 George Street
Oxford OX1 2BQ

I had traveled to Oxford scores of times and lived in Oxford for almost a year before I finally tried this place. I&#8217;ve been attempting to make up for lost time ever since. I have taken all of my foodie friends here, and I&#8217;m fairly certain they&#8217;ll back me up on my assessment.

Things to try:

1. Salted Garlic Chili Ribs & Rice: This is an startlingly spicy/hot dish. Eating the ribs with chopsticks is a challenge, but the effort is worth it. (no pic)

2. Pork and Szechuan Vegetable Rice: My wife and I grew addicted to this dish and spent the better part of a year trying to recreate it at home. We&#8217;ve finally given up and have resigned ourselves to going to Cafe Orient for our fix [see photo below]

{{< figure src="assets/img/pork_and_szechuan_vegetable.jpg" title="Pork and Szechuan Vegetable Rice">}}



3. Spicy Tofu Rice: Again- hot enough to take your head off. If you have never liked tofu- give this a try. It will make vegetarianism seem like a possibility. [see photo below]

{{< figure src="assets/img/spicy_tofu.jpg" title="Spicy Tofu Rice">}}


4. Beef with Ginger & Spring Onion Rice: Recommended to those who don&#8217;t want hot food. (no pic)


And a gratuitous shot of the kitchen&#8230;

{{< figure src="assets/img/cafe_orient_kitchen.jpg" title="Kitchen">}}

