---
title: Back to the US
author: gbilder
type: post
date: 2005-05-31T15:15:13+00:00
url: /2005/05/back-to-the-us/
categories:
  - Travel
---

Speaking at SSP in Boston on Thursday (June 2nd) on the &#8220;Journal of the Future&#8221;. View from VA lounge at the new control tower.

{{< figure src="assets/img/Picture(9).jpg">}}
