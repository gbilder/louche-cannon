---
title: View from Lenox hotel
author: gbilder
type: post
date: 2005-06-01T07:17:03+00:00
url: /2005/06/view-from-lenox-hotel/
categories:
  - Photos
  - Travel
---

I have tried the &#8220;ask for a room with a view trick&#8221; and it has worked yet again. I can&#8217;t believe I spent 10 years of business travel without catching on to this.

View is down Boylston street in Boston.

{{< figure src="assets/img/Picture(10).jpg">}}

