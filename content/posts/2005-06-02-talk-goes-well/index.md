---
title: Talk goes well
author: gbilder
type: post
date: 2005-06-02T15:26:13+00:00
url: /2005/06/talk-goes-well/
categories:
  - Photos
  - Travel
---

It looks like attendance exceeded 200. Naturally- I crashed the pips and the hotel was so keen to get us out of the room that we didn&#8217;t have time for questions. However, a small crowd gathered around me afterwards- they were complimentary, asked good questions and were not shaking their fists. So a good result, I think.

Conference room just before talk&#8230;

{{< figure src="assets/img/Picture(11).jpg">}}

