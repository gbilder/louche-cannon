---
title: Look who showed up
author: gbilder
type: post
date: 2005-06-03T15:30:02+00:00
url: /2005/06/look-who-showed-up/
categories:
  - Personal
  - Photos
  - Travel
---

Allen Renear shows up (unannounced) at SSP. Several late nights of ranting about trust-metrics, document models, management headaches and consulting possibilities. It has been ten years since we last conspired like this.

{{< figure src="assets/img/Picture(12)-1.jpg">}}

