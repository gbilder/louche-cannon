---
title: The lazyweb works
author: gbilder
type: post
date: 2005-06-20T10:45:51+00:00
url: /2005/06/the-lazyweb-works/
categories:
  - Technology
---

After my trash-talk about &#8220;uber-geeks&#8221;, [Leigh Dodds][1] picked up the &#8220;[Subscribe To My Brain][2]&#8221; challenge and produced [this][3] within hours. He even produced a button, which -as everybody knows- automatically turns beta software into a production service&#8230;

It turns out that [Phil Wilson][4], was working on a similar concept and [Danny Ayers][5] was thinking along the same lines.

And I am happy to say that the phrase &#8220;subscribe to my brain&#8221; seems to have turned into a mini-meme (Play Austin Powers theme tune here, start maniacally laughing).

I promised to subscribe to the brain of whoever wrote the requested service, but the problem is I already subscribe to Leigh&#8217;s brain- so the only reward I can offer him is a tin of French sweets sporting an appropriate name:

{{< figure src="assets/img/Picture(5).jpg">}}

Oh, yeah- and I added a &#8220;brain button&#8221; (amongst others) to this site.

{{< figure src="assets/img/brain-1.png">}}


[1]: http://www.ldodds.com/blog/archives/000217.html
[2]: http://www.gbilder.com/blog/?p=40
[3]: http://www.ldodds.com/foaf/brain-subscribe
[4]: http://philwilson.org/blog/2005/06/im-trying-to-subscribe-to-your-brain.html
[5]: http://dannyayers.com/archives/2005/06/19/group-brain-subscription/
