---
title: "In Google We Trust?"
author: gbilder
type: post
date: 2006-12-01
url: /blog/2006/12/in-google-we-trust/
draft: true
categories:
  - Technology
  - Trust
---

Bilder, G, 2006, &#8216;In Google We Trust?&#8217;, _Journal of Electronic Publishing_, vol. 9, no. 1. [<span class="Z3988" title="ctx_ver=Z39.88-2004&rft_val_fmt=info:ofi/fmt:kev:mtx:journal&rft_id=info:doi/10.3998/3336451.0009.101&rtf.genre=journal-article&rtf.date=2006-01-31&rtf.aulast=Bilder&rtf.aufirst=Geoffrey&rtf.auinit=G&rtf.atitle=In Google We Trust?&rtf.jtitle=Journal of Electronic Publishing&rtf.volume=9&rtf.issue=1">10.3998/3336451.0009.101</span>][1]

[1]: http://dx.doi.org/10.3998/3336451.0009.101
