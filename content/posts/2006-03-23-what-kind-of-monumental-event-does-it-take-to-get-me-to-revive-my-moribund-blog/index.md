---
title: What kind of monumental event does it take to get me to revive my moribund blog?
author: gbilder
type: post
date: 2006-03-23T16:20:44+00:00
url: /2006/03/what-kind-of-monumental-event-does-it-take-to-get-me-to-revive-my-moribund-blog/
categories:
  - Photos
---

Seeing [Leigh Dodds][1] wearing a tie. Apparently his children thought it was pretty odd too.


{{< figure src="assets/img/Picture(17).jpg">}}

Oh, yeah. And having somebody point out in their &#8220;Web 2.0&#8221; presentation that you haven&#8217;t updated your blog in half a year. /Me=shamed.

[1]: http://www.ldodds.com/blog/
