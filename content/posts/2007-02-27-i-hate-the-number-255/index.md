---
title: I hate the number 255
author: gbilder
type: post
date: 2007-02-27T11:42:43+00:00
url: /blog/2007/02/i-hate-the-number-255/
categories:
  - Technology
---

I hated it in Pascal and I hate it now in del.icio.us. This might even force me to stop using del.icio.us.

{{< figure src="assets/img/200702271138.jpg">}}


Of course, it isn&#8217;t the number that I really hate- its the programmers who, rather than think of the realistic use cases for a column called &#8220;notes&#8221;, just settle for the default &#8220;biggish computer number&#8221; that pops into their head. You&#8217;d think they would have at least upgraded to 512 or 1024 by now.

Grump.
