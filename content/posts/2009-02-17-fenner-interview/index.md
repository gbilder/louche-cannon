---
title: "Author Identifiers: Interview with Geoffrey Bilder"
author: gbilder
type: post
date: 2009-02-17
url: /blog/2009/02/fenner-interview/
draft: false
categories:
  - Identifiers
  - ORCID
Params:
  ShowReadingTime: true
  syndicate: true
---

In 2009 [Martin Fenner](https://front-matter.io/team) sent me an email asking to interview me about the "Author Identifier" project I was working on for Crossref (later to become [ORCID](https://rcid.org)). I had no idea who Martin was, but he asked intelligent questions and so I answered them. The result is below. And Martin and I have been friends/colleagues ever since.

Fenner, M. (2009). Author Identifiers: Interview with Geoffrey Bilder. [https://doi.org/10.53731/r294649-6f79289-8cw1h](https://doi.org/10.53731/r294649-6f79289-8cw1h)
