---
title: "Structure of the ORCID Identifier"
author: gbilder
type: post
date: 2010-04-16
url: /blog/2010/04/structure-of-orcid-identifier/
draft: false
summary: "This was the document I wrote proposing what was eventually to become the ORCID identifier."
categories:
  - ORCID
  - Identifiers
params:
  ShowReadingTime: true
  syndicate: true
---

In 2010 I was seconded by Crossref for to be ORCID's first director of technology. This was the document I wrote proposing what was eventually to become the ORCID identifier.

[The Structure of the ORCID Identifier](https://docs.google.com/document/d/1awd6PPguRAdZsC6CKpFSSSu1dulliT8E3kHwIJ3tD5o/edit#heading=h.n4gh01ccn1yv)
