---
title: "Disambiguation without de-duplication: Modeling authority and trust in the ORCID system."
author: gbilder
type: post
date: 2011-03-16
url: /blog/2011/03/disambiguation-without-deduplication/
draft: false
categories:
  - Identifiers
  - ORCID
Summary: "At one point there was an impasse in the community over the who would \"own\" an ORCID record and what strategy ORCID should use to \"deduplicate\" records. I wrote this paper to try and cut through the thicket of assumptions and misunderstandings."
params:
  ShowReadingTime: true
  syndicate: true
---

In 2010 I was seconded by Crossref for to be ORCID's first director of technology. At one point there was an impasse in the community over the who would "own" an ORCID record and what strategy ORCID should use to "deduplicate" records. I wrote this paper to try and cut through the thicket of assumptions and misunderstandings.

[Disambiguation without de-duplication: Modeling authority and trust in the ORCID system.](/pdfs/disambiguation-deduplication-wp-v4.pdf)
