---
title: "Identify This!"
author: gbilder
type: post
date: 2011-06-01
url: /blog/2011/06/identify-this/
draft: false
categories:
  - Technology
  - Trust
summary: "Identify This! Identifiers and Trust."
params:
  ShowReadingTime: true
  syndicate: true
---

I try to not be boring about identifiers.

Bilder, G. (2011). Identify This! Identifiers and Trust. Information Standards Quarterly, 23(3), 20. [https://doi.org/10.3789/isqv23n3.2011.05](https://doi.org/10.3789/isqv23n3.2011.05)
