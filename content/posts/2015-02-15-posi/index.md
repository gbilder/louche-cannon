---
title: "Principles for Open Scholarly Infrastructures"
author: gbilder , cneylon, jlin
type: post
date: 2015-02-23
url: /blog/2015/02/posi/
draft: false
categories:
  - Technology
  - Trust
summary: "Jennifer Lin, Cameron Neylon and I wrote a thing."
params:
  ShowReadingTime: true
  syndicate: true
---

Bilder, Geoffrey; Lin, Jennifer; Neylon, Cameron (2015). Principles for Open Scholarly Infrastructures-v1. figshare. Journal contribution. [https://doi.org/10.6084/m9.figshare.1314859.v1](https://doi.org/10.6084/m9.figshare.1314859)
