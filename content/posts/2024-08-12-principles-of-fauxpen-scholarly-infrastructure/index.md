---
title: "The Principles of Fauxpen Scholarly Infrastructure"
author: "Geoffrey Bilder"
type: post
date: 2024-08-12
url: /blog/2024/08/principles-of-fauxpen-scholarly-infrastructure/
draft: false
ShowToc: false
categories:
  - POSI
  - Infrastructure
  - Fauxpen
cover:
    image: "covers/lego.jpeg" # partial path/url
    alt: "A colorful pile of assorted LEGO bricks in various shapes and sizes, featuring white, red, yellow, purple, pink, orange, blue, and brown pieces scattered randomly across the image." # alt text
    #caption: "<banner-image-caption>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
params:
  ShowReadingTime: true
  syndicate: true
---


[Force11](https://force11.org) has posted a video of the presentation I gave at [Force24](https://force11.org/post/force2024-call-for-talk-proposals/).

I discuss [POSI](https://openscholarlyinfrastructure.org)’s limitations and the risks associated with the self-auditing culture that has developed around them. I also talk about recent developments in the community that threaten to slow or even roll back the adoption of open scholarly infrastructure.

- [The Principles of Fauxpen Scholarly Infrastrcuture](https://www.youtube.com/watch?v=DZ2Bgwyx3nU)


