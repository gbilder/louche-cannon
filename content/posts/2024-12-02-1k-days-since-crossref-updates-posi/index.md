---
author: "Geoffrey Bilder"
title: "1000 days since Crossref updated its POSI self-assesment"
draft: false
date: "2024-12-02"
description: "POSI not updated for 1000 days"
tags: ["POSI", "Infrastructure"]
ShowToc: false
ShowBreadCrumbs: false
#weight: 1
Summary: "As of today (2024-12-02), it has been 1000 days since Crossref updated its [POSI](https://openscholarlyinfrastructure.org/) self-assessment. That translates to almost 2 years and 9 months."
cover:
    image: "covers/unease.jpeg" # partial path/url
    alt: "Two black decorative canisters with silver lids from \"Hoxton Street Monster Supplies ESTD 1818\" with the tagline \"Purveyor of Quality Goods for Monsters of Every Kind\". Left canister labeled \"TINNED FEAR: A VAGUE SENSE OF UNEASE\" with description \"Effectively destroys all feelings of ease, creating a rising yet uncertain sense of disquiet. Invaluable for general uses in the home. Guaranteed perfectly pure and genuine.\" Right canister labeled \"TINNED FEAR: ESCALATING PANIC\" with description \"Suitable for all instances where one needs to instill a steadily increasing sensation of pure panic. Particularly effective in crowds. Not to be confounded with Mortal Terror.\"" # alt text
    #caption: "<banner-image-caption>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
Params:
  main_image: "/posts/2024-12-02-1k-days-since-crossref-updates-posi/assets/avatar-favicon.png"
  ShowReadingTime: true
  syndicate: true
  doi: "https://doi.org/10.59347/89nnz-6mt57"
  authors:
    - name: "Geoffrey Bilder"
      orcid: "https://orcid.org/0000-0003-1315-5960"
---

{{< cite-me >}}

<details>
  <summary>COI</summary>
  I was one of the co-authors of the original <a href="https://doi.org/10.6084/m9.figshare.1314859.v1">Principles of Open Schlarly Infrastructure</a>. I also once worked for Crossref- during which time I wrote their first two POSI self-assesments.
</details>

### 1000 Days and counting

As of today (2024-12-02), it has been 1000 days since Crossref updated its [POSI](https://openscholarlyinfrastructure.org/) self-assessment. That translates to almost 2 years and 9 months.

It's not the worst of the [POSI Posse](https://gbilder.com/posi-dashboard/) in this respect.

But it is probably the most worrying, if for no other reason than that Crossref is the largest scholarly infrastructure organization that currently endorses POSI.

Crossref was also the first to endorse POSI, and as such, it set the tone and tempo for the rest of the POSI Posse and their self-assessments.

### Why worry about the rest of the POSI Posse?

Worry because POSI adoption works like a [warrant canary](https://en.wikipedia.org/wiki/Warrant_canary), but for [enclosure.](https://en.wikipedia.org/wiki/Enclosure) If an organization has not refreshed its POSI self-assessment, it should serve as a warning to stakeholders. At best, it means the organization no longer thinks it essential to inform stakeholders of its efforts to meet the commitments in POSI. At worst, it may mean the organization is rethinking its commitment to POSI and planning to enclose scholarly infrastructure on which we all depend.

Worry because the members of the POSI Posse represent a significant percentage of all open scholarly infrastructure.

Worry because POSI has become an essential element of policy initiatives like the [Barcelona Declaration](https://barcelona-declaration.org), which explicitly cites POSI in its [commitments](https://barcelona-declaration.org/commitments/) as an example of following good practices in sustainability and governance.

Worry because, without continuous self-assessment against the specific commitments made in POSI, POSI endorsement will become another form of "open-washing."

Worry because open scholarly infrastructure organizations will be tempted to sacrifice their missions and ideals to survive in a political and policy environment where governments seem likely to slash science and infrastructure funding.

Worry because, as long as critical open scholarly infrastructures are not meeting the commitments in POSI, they are at risk of enclosure. And, as the [original POSI document](https://doi.org/10.6084/m9.figshare.1314859.v1) said:

> “Everything we have gained by opening content and data will be under threat if we allow the enclosure of scholarly infrastructures.”

### Why worry even more?

Worry more because some critical scholarly infrastructure organizations have not endorsed POSI at all. Where is arXiv, bioRxiv, The Centre for Open Science, or The Lens?

Worry more because ORCID has recently refused to endorse POSI. Recently, they issued a lengthy [self-assessment](https://info.orcid.org/wp-content/uploads/2024/10/ORCID-POSI-Self-assessment.pdf) asserting that they meet most of the POSI commitments yet still don’t want to endorse POSI itself. The TLDR is that they don’t want to preclude the option of enclosing their data.

Worry more because several well-known organizations are bending over backwards to describe themselves as [something almost, but not quite, entirely unlike](https://www.goodreads.com/quotes/20153-after-a-fairly-shaky-start-to-the-day-arthur-s-mind) “open infrastructure.” They use phrases like “non-profit infrastructure,” “mission-driven infrastructure,  “shared infrastructure,” and “researcher-led infrastructure.”
 It’s almost like they don’t like the word “open.”

### What to do?

[IOI’](https://investinopen.org/)s Director of Programs [Katherine Skinner](https://investinopen.org/about/team/katherine/) spoke at the HathiTrust 2024 Member Meeting. The title of her talk was “The emperor’s new clothes: Common myths hindering open infrastructure.” Please [read or watch it.](https://investinopen.org/blog/the-emperors-new-clothes-common-myths-hindering-open-infrastructure/) She makes several profound observations, but starts with one meta-point that is particularly relevant here:

“**calling out examples is risky business.”**

To paraphrase: calling out open infrastructure organizations like this can feel churlish and harmful.

Some open infrastructure organizations are struggling. They might be scrambling to seek funding. They might be in the middle of a reorganization. They might be barely keeping the lights on.

We may worry that sowing any doubts about them might exacerbate their problems by encouraging their clients to move to seemingly more stable commercial options.

So, we confine our concerns to private back channels where it is, by definition, almost impossible to summon the collective action needed to address the issues.

Our coyness will be our undoing.

We must drag these conversations into the open.

Jennifer Lin, Cameron Neylon, and I drafted the original principles in 2015 to address a trend we had noticed: Services that the scholarly community depended on were increasingly taking directions that seemed antithetical to the community’s interests.

Today's kids might call this the “enshittification” of scholarly infrastructure. This trend hasn’t slowed down. Indeed, with the deluge of proprietary AI tools targeting researchers, it appears to be accelerating.

We designed the commitments outlined in POSI to help ensure that the scholarly community remains in control of the infrastructure on which it depends.

We wanted POSI to be a tool that allowed librarians, funders, administrators, policymakers, and researchers to assess the relative risk of adopting various infrastructure tools.

We also hoped that funders might use POSI to help target funding to areas where research was in danger of becoming too dependent on enclosed and inequitable services.

In other words, we wanted stakeholders to take an active and constructive role: analyzing, critiquing, and shaping the services and organizations they depended on.

I don’t think we ever expected POSI to become a self-assessment checklist.

And we certainly didn’t expect other organizations to emulate Crossref’s initial attempt to distill the self-assessment using the traffic-light system.

But here we are.

As of today (2024-12-02), twenty-two organizations have decided to endorse POSI.

This represents grassroots growth; no group is actively promoting POSI.

However, this is only a good development if we don’t treat the endorsement of POSI as a virtue merit badge.

Or as a tick-box list of one-off accomplishments that can be safely ignored once completed.

As Martin Eve pointed out [in a blog post in 2023](https://eve.gd/2023/07/28/rules-vs-principles-in-posi/), people tend to forget that these are “principles,” not “rules.” This means that they require contextual interpretation, and these interpretations need to evolve as the scholarly infrastructure landscape evolves.

This is why the self-assessments are only worth doing if there is evidence that they are part of a continuous process of reflection and reevaluation of the principles in light of new challenges.

Which brings me back to Kathrine Skinner’s point – that, as stakeholders, we have to be more willing to take a public, active role in preventing the enclosure of the scholarly infrastructure we rely on.

Of course I am concerned that most of the members of POSI Posse have not updated their self-assessments in over a year.

But what **really** concerns me is that nobody has called them out on this.

We can’t take self-assessments at face value.

We can’t rely on backchannel conversations to fix issues that require collective action.

Pick an infrastructure organization you depend on.

If they have not endorsed POSI, ask them why.

If they have endorsed POSI, read their self-assessments with the same scrutiny you would a research paper you were about to cite or a contract you were about to sign.

If they haven't updated their POSI self-assessment in more than a year, ask them why.

I’ve created a [dashboard](https://gbilder.com/posi-dashboard/) to help with this.

If you are ignored or don't get reasonable answers, contact board members or advisory groups and let them know your concerns.

But most of all, do this publicly.

And let's see if next year, the POSI endorsement update list looks more reasonable.
