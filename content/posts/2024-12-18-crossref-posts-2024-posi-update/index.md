---
author: "Geoffrey Bilder"
title: "Crossref updates its POSI self-assessment after 1,005 days"
draft: false
date: "2024-12-18"
lastmod: "2025-01-14"
description: "POSI updated after 1,005 days"
tags: ["POSI", "Infrastructure"]
ShowToc: true
ShowBreadCrumbs: false
Summary: "After 1,005 days, [Crossref updated its POSI self-audit](https://www.crossref.org/blog/2024-posi-audit/)."

cover:
    image: "covers/cultivate-doubt.jpeg" # partial path/url
    alt: "A yellow railway bridge with the text \"CULTIVEZ DU DOUTE\" (French for \"Cultivate doubt\") written in graffiti on it. The bridge spans over green vineyard fields with residential houses visible in the background." # alt text
    #caption: "<banner-image-caption>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
params:
  ShowReadingTime: true
  syndicate: true
  doi: "https://doi.org/10.59347/zrdrv-2t618"
  authors:
    - name: "Geoffrey Bilder"
      orcid: "https://orcid.org/0000-0003-1315-5960"


---

{{< cite-me >}}

## Disclosure

I was a co-author of the original [Principles for Open Scholarly Infrastructures](http://dx.doi.org/10.6084/m9.figshare.1314859). I also worked for Crossref and wrote its [first](https://www.crossref.org/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure/) and [second](https://www.crossref.org/blog/posi-fan-tutte/) POSI self-assessments. However, I’ve based this analysis on public information.

## TL;DR

After 1,005 days, [Crossref updated its POSI self-audit](https://www.crossref.org/blog/2024-posi-audit/). Crossref has made some important progress in increasing board diversity, establishing financial reserves, removing a closed source technical dependency from its system, and ensuring that IP is not a barrier to forking Crossref services.

This is good news. But I remain concerned.

The report has some important omissions and transparency issues. It fails to explain why Crossref took nearly three years to update its POSI assessment and does not mention a significant regression in its POSI commitments during that time.

The report also does not address some important, unresolved questions like “Is it possible for a membership trade association like Crossref to be ‘stakeholder governed’ if its stakeholders are not all members of ‘the trade’ in question?”

I’ll provide a more detailed list of my concerns below.

But, before I do that…

## On stakeholder critiques

In a [previous post](https://gbilder.com/posts/2024-12-02-1k-days-since-crossref-updates-posi/) I made reference to  [the point made by IOIs Katherine Skinner](https://investinopen.org/blog/the-emperors-new-clothes-common-myths-hindering-open-infrastructure/) about the research community’s reluctance to publicly discuss the challenges of open scholarly infrastructures.

Often we’re reluctant because we’re afraid that we might undermine confidence in already weakened services and that our criticisms might make matters worse.

This isn’t something that we have to worry about with Crossref.

Sometimes we worry about creating headaches for past colleagues, or about alienating potential future colleagues.

I do worry about that.

And, in this case, I also worry that, by critiquing Crossref’s self-audit update, I’ll be discouraging other POSI adopters from revising theirs for fear of similar treatment.

Or worse – that this critique will discourage other organizations from adopting POSI in the first place.

One of my proudest moments was when the Crossref board voted to adopt POSI in 2020.

As I write this, 22 organizations have adopted the principles.

I want to see POSI thrive. I want to see the POSI Posse thrive.

But it would be a shame if these self-audits become another phatic managerial communication ritual – like issuing vague, unactionable, and unmeasurable mission statements.

Or boilerplate bromides designed to lull people into complacency.

Because that **will** alienate stakeholders. And undermine the value of POSI endorsements.

If other POSI self-audits follow the pattern set by this one, I’m afraid that is exactly what will happen.

## Omissions, transparency issues, and open questions

The most obvious omission from Crossref’s 2024 self-audit is that it doesn’t answer these questions:

> [!QUESTION]
> Why did it take Crossref over 1,000 days to update its self-audit?

> [!QUESTION]
> Why did Crossref decide to update its self-audit now?

> [!QUESTION]
> What are Crossref’s criteria for deciding when to provide updates, and when can stakeholders expect the next one?

Maybe Crossref was unaware of the blog post I wrote a week before, marking 1,000 days since it had last updated its POSI self-audit. Or of the subsequent reaction to the post on social media. Or maybe Crossref was aware, but did not want to draw more attention to it. Or maybe Crossref was aware, and just didn’t care.

Regardless, this suggests Crossref hasn’t fully internalized what it means to be “open.”

“Openness" implies a willingness to share information transparently. Even when that information is inconvenient or embarrassing. Doing this shows integrity– by demonstrating readiness to accept responsibility and address concerns without taking offense or being defensive.

And, even without explicitly drawing unwelcome attention to my post, Crossref could have at least addressed the broader concerns about the lengthy delay and what it says about Crossref's regard for stakeholders.

Given the attention the post gained on platforms like Mastodon and Bluesky, it’s clear these issues resonate with Crossref's community.

Had Crossref said something as simple and open as:

> “We’re sorry it took so long; we should have updated our POSI self-audit sooner. Our criteria for updates are X and we’ll update you every Y days from now on”

It would have been enough. It would have shown integrity, and Crossref stakeholders would likely be happy to move on to other issues.

But it didn’t.

And Crossref can’t use the excuse that nothing of significance happened during those 1,005 days.

### Stakeholder governance

In 2021 the Crossref membership elected a funder to the Crossref board of directors, but the funder stepped down mid-term and the nominating committee chose to fill its seat with another publisher. This means that, in 2023, Crossref regressed into non-compliance with stakeholder governance, a critical POSI commitment. Crossref did not report this at the time, and it fails to mention it in the 2024 update, instead stating that:

> “We’ve been yellow and we’re still yellow…”

...implying continuity between the 2021 and 2024 self-audits when the reality was there was a regression.

That is not how self-audits are supposed to work and presenting the sequence of events this way shows a worrying lack of transparency and accountability.

> [!QUESTION]
> Why didn’t Crossref disclose that, for over a year, it had regressed on a crucial POSI commitment? Why didn’t Crossref report the regression at the time and why didn't it even acknowledge this regression in its 2024 self-audit?

Crossref explains that it still has not met the commitment to “stakeholder governance” because:

>  …being fully stakeholder-governed is challenging

This is true, but the 2024 self-audit doesn’t really explain why.

What everybody knows as “Crossref” is the trading name of the Publishers International Linking Association (PILA). PILA is a non-profit, but PILA is not a “charitable organization” (a “[501(c)(3)](https://www.irs.gov/charities-non-profits/charitable-organizations/exemption-requirements-501c3-organizations)” under the US tax code), instead it is a “business league” (a [501(c)(6)](https://www.irs.gov/charities-non-profits/other-non-profits/business-leagues) under the US tax code). This has important implications for governance because only “members” can govern Crossref and Crossref membership is restricted to those “of the same trade, business, occupation, or profession.”

While Crossref has always been admirably catholic in its definition of what constitutes “a publisher” (for example, funders can be Crossref members because they “publish” grants), there will always be many Crossref stakeholders who are not in “the trade,” no matter how much Crossref stretches the meaning of “publisher.”

Crossref metadata consumers are notably excluded from this arrangement. Initiatives like the [Barcelona Declaration](https://barcelona-declaration.org) highlight infrastructures such as Crossref as essential to open research evaluation, underscoring the need for metadata consumers to be represented in Crossref’s governance.

There are also certain kinds of stakeholders who cannot join membership organizations of any type. Some government and intergovernmental agencies, for example.

Which raises some questions:

> [!QUESTION]
> Is it possible, in the context of POSI, for a membership trade association to be “stakeholder governed?”

> [!QUESTION]
> If not, will Crossref consider adopting a different organizational structure?

### Create financial reserves

[To be fair](https://www.youtube.com/watch?v=jv7jcciKB_s), Crossref didn't just bury bad news. It also buried good news.

In the 2024 update, Crossref mentions that in 2023, it completed the creation of its contingency fund.

In 2023.

And apparently Crossref thought this also didn't warrant updating stakeholders.

Ask any POSI adopter which commitment is going to be the hardest for them to meet, and they will likely flag this one.

It commits POSI adopters to create a contingency fund that covers operating expenses for a long enough period to allow for an orderly wind-down of the organization and the infrastructure that it runs.

Crossref, by almost any measure, is one of the largest and most financially stable of the organizations that have adopted POSI, and meeting this commitment was a stretch even for them.

So you would think meeting the commitment to create a financial reserve would be something to tell its stakeholders and to celebrate.

Over a year ago. When it happened.

> [!QUESTION]
> Why didn’t Crossref inform stakeholders that it had met a crucial POSI commitment over a year ago?

When we drafted POSI, we knew meeting this commitment would be a difficult hurdle but hoped it would spark some discussions about how meeting the commitment could be made less onerous, short of watering it down.

There is nothing in POSI that says that each organization has to raise these funds on its own.

This is a good example of where collective action could help instead.

> [!QUESTION]
> Can the POSI Posse work with funders to explore the creation of a collective insurance pool that will cover POSI adoptees, so that each one doesn’t have to individually try and create a financial reserve fund on its own?

### Open Source

In 2024, Crossref made an important move from the proprietary Oracle database to the open-source PostgreSQL. This is, as Crossref notes, an important step in its quest to be able to release all of its code as open source – a key insurance provision in POSI.

In its [2022 self-audit](https://www.crossref.org/blog/posi-fan-tutte/), Crossref highlighted that the move to open source was likely to be a multi-year effort.

But the 2024 self-audit does not provide an estimated timeline.

Maybe it hasn’t changed.

But without any sort of rough timeline, it is hard for stakeholders to understand whether or not Crossref is making reasonable progress on this commitment.

> [!QUESTION]
> What is the estimated timeline for Crossref meeting its commitment to open source its code?

The Crossref self-audit update describes the move off Oracle and onto the open-source database PostgreSQL as:

> …an important step towards paying down technical debt and moving the system fully into the cloud.

Which is reasonable. But it does raise an obvious question. Cloud services offer proprietary functionality that can lock in users in almost exactly the same way that proprietary databases like Oracle can.

> [!QUESTION]
> What precautions is Crossref taking to ensure that it is not using proprietary, closed "cloud" functionality?

And since we are discussing dependencies, it is probably also worth asking about a few other not-so-obvious dependencies that affect Crossref.

The first is an organizational dependency.

Crossref is a DOI registration agency. It is dependent on three other organizations that have not endorsed POSI. Namely:

- [The DOI Foundation](https://www.doi.org/)
- [The DONA Foundation](https://www.dona.net/)
- [Corporation for National Research Initiatives (CNRI)](https://www.cnri.reston.va.us/)

> [!QUESTION]
> What risks do Crossref’s dependencies on organizations who have not adopted POSI present to Crossref's ability to meet its POSI commitments?

> [!QUESTION]
> Should Crossref and the POSI Posse be working together to try to get these organizations to adopt POSI as well?

The second is a technical dependency.

The DOI system is based on the [Handle](https://www.handle.net/) technology stack developed by CNRI. The [Handle system license](https://www.handle.net/HSj/hdlnet-2-LICENSE.pdf) is not [OSI-approved](https://opensource.org/licenses).

> [!QUESTION]
> What implications does Crossref’s dependency on the Handle system have for Crossref's ability to meet its POSI commitment that "all software and assets required to run the infrastructure should be available under an open-source license?"

### Transparent governance

Crossref doesn’t talk about transparent governance in the 2024 self-audit, but it should have.

After adopting POSI, Crossref added new sections to its website to provide stakeholders with transparent information about its [governance](https://www.crossref.org/board-and-governance/), [operations, and finances](https://www.crossref.org/operations-and-sustainability/). These sections are an excellent resource, and everything I’ve highlighted here can be gleaned from these pages.

The board and governance page, for example, includes links to the latest “[board motions passed](https://www.crossref.org/board-and-governance/).” Even before adopting POSI, Crossref made a habit of updating this page with motions from recent board meetings -- typically within a few days of the meeting.

This is also where you can find motions related to POSI, such as this one passed as part of the nominating committee’s instructions during the [November 2023 meeting](https://www.crossref.org/board-and-governance/):

> Encourage and recruit Crossref members who are research funders to apply to the Board.

Crossref has three board meetings a year- in March, July, and November.

The last board motions listed are from the March 2024 meeting. This means that board motions from the two board meetings since March 2024 are missing.

> [!QUESTION]
> Does Crossref plan to continue posting board motions immediately after the board meetings?

> [!NOTE] Update 2025-01-11
> It appears Crossref updated the board motions sometime after [December 18th, 2024](https://web.archive.org/web/20240915000000*/https://www.crossref.org/board-and-governance/).

> [!QUESTION]
> Has Crossref considered adopting [ORCID’s practice of publicly posting board meeting summaries](https://info.orcid.org/board-meeting-summaries/)?

## What next?

First, I want to encourage those who have adopted or are about to adopt POSI to make their self-audits as open, honest, and self-critical as they can.

Stakeholders will be forgiving if you are having a hard time meeting the POSI commitments and transparently name those challenges. But they won’t be if it looks like you are trying to hide something.

The only way we are going to educate stakeholders about the challenges involved in building and running open scholarly infrastructures is if we are candid. And we need educated stakeholders to support the collective action required to support open scholarly infrastructure.

And, while I think self-audits are important for educating and building trust with stakeholders, I don’t think they are enough on their own.

Even professional external audits [can fail spectacularly.](https://www.superfastcpa.com/what-is-an-audit-failure/#:~:text=One%20well%2Dknown%20example%20of,accounting%20firms%20at%20the%20time.)

Self-audits are, well... self-audits.

It's not that we should suspect that self-audits are inherently dishonest.

However, self-audits rely on organizational self-awareness and a culture that supports self-criticism.

All organizations have ingrained internal ideologies, so all organizations have blind spots.

And many organizations have “[third rail](https://en.wikipedia.org/wiki/Third_rail_\(politics\))” topics that everybody in the organization is afraid to touch.

One problem with most audits is that very few people outside the audited organization have the information required to scrutinize the audit effectively.

But this isn't true of POSI.

The themes and individual objectives outlined in POSI are focused on openness. These are things that stakeholders can verify. Here is a small sample:

* Do they have a governance structure that reflects all the infrastructure stakeholders?
* Can you examine their board and committee documents?
* Can you look at their publicly available financial statements? Do they make sense?
* Is their source code open? Can you actually do anything with it, or is it a tangled, undocumented mess?
* Do their operations depend on source code or other organizations that are not open?

So I want again to encourage stakeholders to scrutinize the POSI self-updates of organizations they rely on. We increasingly depend on these organizations; if they fail or become enclosed, it will affect virtually all research stakeholders.

I particularly want to call on funders and policy makers to take an active role in scrutinizing these self-audits.

The open research initiatives they are funding are dependent on there being an open scholarly infrastructure to build on.

I look forward to seeing the other POSI Posse self-updates.

I’ll leave it to other stakeholders to step in and scrutinize them.

I’ll focus instead on those who haven’t yet endorsed POSI.
