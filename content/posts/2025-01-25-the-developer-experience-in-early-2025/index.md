---
author: Geoffrey Bilder
title: "The Developer Experience in Early 2025"
date: "2025-01-25"
lastmod: "2025-01-25"
draft: false
description: "The Developer Experience in Early 2025"
Summary: "Imagine pair programming with an eccentric software developer…"
tags: ["ml","ai","development"]
ShowToc: true
ShowBreadCrumbs: false

cover:
    image: "covers/cdg-terminal-2f.jpeg" # partial path/url
    alt: "CDG airport, terminal 2f: Urban gardening installation with rows of gray plastic pots arranged in neat lines on a concrete surface within what appears to be a brutalist architectural setting. The concrete structure has several levels with pillars and overhead cables visible." # alt text
    #caption: "<banner-image-caption>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
params:
  ShowReadingTime: true
  syndicate: true
  doi: "https://doi.org/10.59347/8nwg7-hbh15"
  authors:
    - name: "Geoffrey Bilder"
      orcid: "https://orcid.org/0000-0003-1315-5960"

---

{{< cite-me >}}

Imagine [pair programming](https://en.wikipedia.org/wiki/Pair_programming#:~:text=Pair%20programming%20is%20a%20software,two%20programmers%20switch%20roles%20frequently.) with an eccentric software developer…

 OK, good point—  I mean more eccentric than usual.

…anyway— they have fantastic long-term memory, but terrible short-term memory.

They are polite to the point of obsequiousness.

They can type thousands of words per second and they like to make fifty unrelated code changes at once.

They can write the most complex algorithms flawlessly, yet they routinely forget to import required libraries.

They take a shot of vodka every twenty minutes and get progressively drunker until they are practically paralytic and can't remember a thing. You have to force them to take a nap and recover for a few hours before continuing with your pair programming.

After a few days of this— you  ask the programmer to pin some notes to their monitor which say things like:

- “remember you are working on project X”
- “only change one function at a time, then ask for advice”
- “don’t forget to import needed libraries”
- “don’t forget to remove imports for unneeded libraries”
- “don’t drink vodka during work hours”

This seems to make things better, but as soon as you turn your back, they remove all the notes, crumple them up,  swallow them, and deny the notes ever existed in the first place.

After a fortnight you discover that they somehow embedded an entire implementation of “[Tetris](https://en.wikipedia.org/wiki/Tetris)” in the config management module of the program that you were writing.

Oh, and all the time you are programming together you keep yourself warm by burning a pile of money.
