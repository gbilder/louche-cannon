---
author: Geoffrey Bilder
title: "Comments on Proposed POSI 2.0 Revisions"
date: "2025-03-05"
lastmod: "2025-03-05"
draft: false
description: "Comments on Proposed POSI 2.0 Revisions"
Summary: "The POSI Adopters1 called for feedback on their Proposed POSI 2.0 revisions. You might not be surprised to know that I have opinions."
tags: []
ShowToc: true
ShowBreadCrumbs: false

cover:
    image: "covers/peace.jpeg" # partial path/url
    alt: "Large pink peace symbol leaning against a stone wall, alongside a collection of discarded items including purple foam shapes, a smaller multicolored peace symbol, cardboard boxes (one labeled \"CAMINO REAL TEQUILA\"), plastic crates, and packaging materials arranged on tiled pavement." # alt text
    caption: "<banner-image-caption>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: false # only hide on current single page
params:
  ShowReadingTime: true
  syndicate: false
  doi: "https://doi.org/10.59347/tph3y-tkf88"
  authors:
    - name: "Geoffrey Bilder"
      orcid: "https://orcid.org/0000-0003-1315-5960"


---

{{< cite-me >}}

## Background

On January 22, the POSI Adopters[^1] called for feedback on their Proposed POSI 2.0 revisions.

In case you are not familiar with [The Principles of Open Scholarly Infrastructure](https://openscholarlyinfrastructure.org) (POSI) , they are essentially a "[Ulysses Pact](https://en.wikipedia.org/wiki/Ulysses_pact)" that aims to make it harder for organizations to "[enshitify](https://en.wiktionary.org/wiki/enshittification)" essential  scholarly infrastructure.

As a coauthor of the [original Principles](https://doi.org/10.6084/m9.figshare.1314859.v1), I want to see POSI adapt to address the ever-evolving challenge of preventing the commons from being enclosed for commercial exploitation or turned into exclusively [club goods](https://en.wikipedia.org/wiki/Club_good).

This is largely why my coauthors and I handed them off to the community care of the open and transparent “POSI Adopters.”

And though I am not affiliated with any members of the POSI adopters anymore, you might not be surprised to know that I have opinions.

The POSI Adopters asked the public to submit their comments via a web form, which I have done.

It is a pity that the POSI Apopters did not provide a venue where the public could have commented on the proposals and discussed them more openly.

But at least I can post what I submitted. If anybody else made comments on the proposed POSI changes, I'd encourage them to make them public as well.

To make it easy to follow, I have copied the text of each of the proposed revisions and then commented on them afterwards.

My comments  appear in notes like this:

> [!NOTE]
>
> My comments

After my comments on all the specific proposals,  I make some additional suggestions.

## The Proposed 2.0 revisions and commentary

#### Scope and Application to Types of Organizations

##### Proposed Change/Modification:

Review the principles to ensure they are appropriately scoped and applicable to different types of organizations (e.g. government agencies, a department that is part of a larger organization) and initiatives and provide guidance on how organizations and initiatives can indicate when certain principles don’t apply and how this affects the self-assessment.

##### Notes:

● Consider whether the principles are general enough to be relevant for a range of different types of organizations and initiatives (e.g. volunteer supported services, services that are part of a larger organization, government agencies, or standards organizations.

> [!NOTE]
>
> The most frequent concerns I’ve encountered in this context are:
>
> - POSI is overly focused on scholarly infrastructure; much of it could be adapted to general open infrastructure with some modifications.
>
> - POSI is overly focused on membership organizations.
>
> There’s no reason why other industries can’t adopt POSI to suit their needs. POSI adopters might even be willing to provide guidance. However, it’s already challenging  to make POSI applicable to the diverse range of players in scholarly communication. I’m concerned that attempting to expand it further could either make it overly complex, too ambiguous, or both.
>
> Initially, I was sympathetic to the second concern. It’s undeniable that the original principles were primarily written by people familiar with “membership organizations.” Consequently, many of the examples we highlight (particularly in the original blog post) are related to membership organizations.
>
> But there are at least two senses of the words "member" and "membership".
>
> - **Implicit membership** is categorical or statistical (you belong to a set by virtue of a characteristic, like pétanque enthusiasts)
>
> - **Explicit membership** is formal and intentional (you actively join and are recognized as a member, like a pétanque club)
>
> And if you examine the actual text of the principles, the word "membership" only appears three times- the first two times in the "implicit" sense (members of the scholarly community) and the third time in the "explicit" sense (as in "membership fees"). Even in the last case,  it is just providing one example of possible revenue sources that do not involve "charging for data."
>
> And the 1.1 revision made the issue even less of a concern by replacing the phrase "Non-discriminatory membership" with "Non-discriminatory participation or membership"
>
> So it isn't really clear what text people find inapplicable to other organization types (the call for comments doesn't really give examples) and, so I've come to think this concern is overblown.
> Having said that, it might be useful if the POSI site provided a special section with examples of how current POSI adoptors have interpreted the principles for their particular contexts.

#### Add a distinct principle on Transparent Operations to distinguish it from Transparent Governance, and clarify that transparent governance involves openly providing documents and policies

Proposed Change/Modification:

- Add a distinct principle about transparent operations to distinguish it from transparent governance. Define what is meant by transparent operations.
- Clarify that transparent governance includes making governing documents and policies open [sic] available.

##### Notes:

- Transparent governance refers to the openness in the decision-making processes, governance structure, selecting representatives for governance and the mechanisms of how decisions are made. For example, this could include making governance documents
  (e.g. bylaws, articles of incorporation, mission statements, board meeting summaries and
  approved motions), and policies and procedures (e.g. election processes) openly available.
- Transparent operations could encompass aspects like making detailed financials, fees, business models, operating policies and procedures, and strategic and product roadmaps openly available.
- Consider how the principle can help demonstrate organizational accountability and openness in day-to-day operations.

> [!NOTE]
>
> The 1.1 revisions of POSI created a regression, so this can largely be resolved by reverting to the original text of the 1.0 principle.
>
> In the 1.0 version of the principles, under the *Governance* section, there was the principle that read:
>
> > **Transparent operations**
>
> This was modified in the 1.1 revisions to read:
>
> > **Transparent governance**
>
> Which effectively made the entire section only about governance instead of about governance *and* operations.
>
> This is one of several instances where the 1.1 revisions have made POSI less clear and more contentious. Another notable example is where the phrase:
>
> > “data related to the running of the research enterprise should be community property”
>
> was replaced with:
>
> > “data related to the running of the scholarly infrastructure should be community property.”
>
> As ORCID correctly pointed out, the latter could potentially encompass purely operational (and often private) data, such as HR records, passwords, and so on. This, of course, is not what was intended.

#### Clarify and strengthen the Principle of the Living Will

##### Proposed Change/Modification:

- Clarify this principle so that it applies not just when an organization is wound down but to any transfer of assets, data and services to another organization whether planned or not.
- Clarify this principle to encourage organizations to constrain the types of successor organizations they are able to transfer operations and assets to, or to highlight how these restrictions apply to the type of legal entity they are.
- Strengthen this principle with more detail about how successor organizations should be defined and whether dissolution and liquidation clauses are necessary.
- Clarify this principle by providing more details of what a “Living Will” should include and
  examples of existing plans.

##### Notes:

- This principle aims to promote transparency and preparedness, ensuring stakeholders are aware of the conditions that would lead to an organization’s closure and how assets would be managed.
- A comment from the feedback on the 1.1 revision of the principles suggested the principles include a dissolution and liquidation clause similar to: "In case of liquidation, the assets can only be distributed to another non-profit entity without private shareholders, who have a similar dissolution and liquidation clause."
- There are different ways this principle can be met. Certain types of organizations will be subject to laws requiring that assets be dispersed to another non-profit with a similar mission. However, a clear statement on dissolution and liquidation in bylaws, consistent with the governing law of an organization, would be useful.

> [!NOTE]
>
> The primary concern here is that a POSI-endorsing organization might transfer its assets to a non-POSI endorsing entity that could subsequently enclose them.
>
> This is a certainly a risk that should be addressed, but it is also an opportunity for POSI to highlight that, if an organization has at least met three of the principles for POSI compliance:
>
> - open data
> - open source
> - open patents
>
> then this "enclosure-through-transfer-or-inheritance" scenario becomes virtually impossible because the community can always choose to “[fork](https://en.wikipedia.org/wiki/Fork_(software_development))” the infrastructure.
>
> In this respect, POSI serves as a “[poison pill](https://www.investopedia.com/terms/p/poisonpill.asp),” making it highly undesirable for hostile takeovers or inheritances.
>
> Yes, there are still certain assets that the community would not be able to take with them when forking, such as “domain names,” “trademarks,” and “private data.” However, this is also true of large open source projects, and these limitations have not prevented forks of numerous significant open source projects (e.g., OpenOffice->LibraOffice, Redis->Valkey, ElasticSearch->OpenSearch, MySQL->MariaDB, Terraform->OpenTofu).
>
> So yes, it seems useful for POSI to define principles around successor organizations and the transfer of assets in order to constrain the behavior of adopters who haven’t yet met the commitments to open data, open source, and open patents.


#### Formal Incentives to Fulfill Mission and Wind Down

##### Proposed Change/Modification:

- Revise this principle to focus on organizations and initiatives regularly reviewing their value to the community to assess if they are still necessary.
- Remove the reference to “direct incentives to deliver on the mission and wind down”

##### Notes:

- There is some skepticism about the practicality of this principle, as it’s unclear what formal incentives would encourage an organization to cease operations voluntarily.
- Many adopters have interpreted this as the need to regularly review community demand for their services and committing to not “outstay their welcome” should demand no longer exist.
- Feedback with any examples of formal or direct incentives for an organization to fulfill its mission and wind down is welcome.

> [!NOTE]
>
> Anybody who has tried to setup a new infrastructure organization will have met a common and understandable concern amongst those who would be on the hook to fund it- a concern that is often referred as "[The Shirky principle](https://medium.com/@mesw1/the-shirky-principle-791046584763#:~:text=The%20Shirky%20Principle%20states%2C%20“Institutions,end%20up%20maintaining%20or%20even)"
>
> > institutions will try to preserve the problem to which they are the solution
>
> And in the case of mission-driven organizations, this can seem particularly pernicious because it means that, at some level, they have an interest in never fulfilling their mission.
>
> So funders are understandably cautious about committing to funding something that might last "forever."
>
> But the problem with the Shirky formulation (and variations of it [^2]) is that it makes the dynamic sound sinister when there it is in fact driven by entirely innocent, human, and altruistic concerns.
>
> - We don't want to fire colleagues and friends whose livelihood depends on the organization’s existence.
>
> - We don't want to lose our own jobs.
>
> It is a case of perverse incentives.
>
> So one of the core ideas behind POSI was to try to encourage a humble organizational culture that recognized its mission was always more important than its existence.
>
> The notes on the proposed POSI revision state that there is "skepticism about the practicality of this principle, as it’s unclear what formal incentives would encourage an organization to cease operations voluntarily".
>
> First, we should challenge the idea that just because we don't know how to do it yet, it should not be in the POSI principles.
>
> To be blunt, we don't have good examples of widely replicable sustainability models for open scholarly infrastructure either.
>
> And we also don't have examples of un-subvertible governance structures for organizations.
>
> I could go on.
>
> Part of the point of POSI is for the open infrastructure community to directly acknowledge and address these challenges instead of pretending they don't exist.
>
> But back to the question: "What are examples of formal incentives that would encourage organizations to cease operations voluntarily?"
>
> Pay them to.
>
> Introduce internal incremental reward structures that encourage staff to continually identify and remove accidental complexity and services that have ceased to contribute meaningfully to the mission.
>
> Or more radically...
>
> Guarantee each employee a year of salary should they collectively manage to figure out how to solve the problem the organization was designed to solve without needing the organization.
>
> How?
>
> Use the contingency fund.
>
> Or maybe work with funders to collectively build a mission-completion bounty fund?
>
> The point is that the tendency toward institutional self-preservation is a valid concern.
>
> Commercial organizations have a perpetual reason to exist— which is to return a profit to their investors. They have incentives to encourage this.
>
> Non-profit, mission-driven organizations should not have incentives to exist in perpetuity. And they should have incentives to discourage it.
>
> It is important that POSI retain this principle. We might not know how to do this at the moment, but by keeping this in the principles it forces us to think about the problem instead of ignoring it.

##### Expanding the Concept of Resources in the Sustainability Section to IncludeVolunteers

##### Proposed Change/Modification:

- Add a separate principle to address volunteer (and other in-kind) resources, not just financial resources.

##### Notes:

- For organizations that rely heavily on volunteers, these individuals are a critical resource that should be recognized in sustainability planning.
- Acknowledging volunteers as a key resource emphasizes the importance of maintaining and supporting this workforce as part of the organization's long-term viability.

> [!NOTE]
>
> This is a good idea. It is particularly important to document the role of volunteers in sustaining infrastructure organizations because:
>
> - It highlights otherwise invisible labor
> - Volunteer labor is an important source of risk (e.g. burnout, changing economic conditions that discourage volunteering)
> - Reliance on volunteer labor can reduce diversity of stakeholder participation (e.g. unpaid intern syndrome). It would be useful if POSI also encouraged organizations to document what they do to mitigate this danger (e.g. offer stipends, training, etc.)
>
> POSI should also call on organizations to highlight other labor dependencies that are typically not documented. For example:
>
> - Reliance on employees who are on sort-term contracts
> - Reliance on contractors
> - Reliance on intermediaries (e.g. sponsoring organizations, service providers, aggregators )
>
> All of these working arrangements can obscure the true costs and risks associated of running infrastructure.

#### Clarify “Time-Limited Funds are Used Only for Time-Limited Activities” to account for the length of time that it can take to reach sustainability

##### Proposed Change/Modification:

● Clarify the principle, or add information to the FAQ, to clarify that it can take a long time to reach financial sustainability and that time-limited funds could be used to build capacity or implement a sustainability model.

##### Notes:

- There are multiple paths to financial sustainability and time-limited funds like grants can be used for building capacity to reach sustainability and that this process can take a long time.

> [!NOTE]
>
> It’s unclear why financial sustainability is being singled out here, as it may take considerable time to fulfill **any** of the POSI commitments. Different organizations will face varying challenges in meeting these commitments. For instance, Crossref has met its financial commitments but has yet to fully open-source its code due to the size, age and complexity of its codebase.

#### “Goal to Create Financial Reserves” should call for there to be a policy for the level of reserves

##### Proposed Change/Modification:

- Clarify the principle to call for a policy on the level of financial reserves with the actual level of reserves determined by the organization's or initiative's governing body .

##### Notes:

- Organizations, initiatives and services vary greatly in their size and scope and policies on financial reserves should reflect this.
- Specifying a fixed amount for reserves is too inflexible.

> [!NOTE]
>
> The most important thing for POSI to highlight is that conventions that might apply for standard commercial and non-profit organizations are almost certainly not appropriate for infrastructure organizations.
>
> The reason for this is simple: an infrastructure organization, by definition, will have other organizations dependent on it. Furthermore, infrastructure  functionality is likely to be deeply embedded in the systems of those dependent organizations.
>
> So we are not talking just about giving the infrastructure organization enough time to wind down; we are talking about giving the community that depends on said infrastructure enough time to extricate itself from dependency on said infrastructure.
>
> This is why, in the  original POSI principles, we suggested a year as an example timeline when the convention for many other organizations is often between three and six months.
>
> We heard several jeremiads about how "one year was too long," but frankly, given the ubiquity of some of the infrastructure organizations that have adopted POSI, one year is probably way too short.
>
> In any case, the principles are supposed to be principles— so , yes, each organization should pick an appropriate time scale. But POSI itself should emphasize that the expectations for winding down infrastructure are fundamentally different than those for other kinds of organizations.

#### Addition of Separate Item on Interoperability and Open Standards Under 'Insurance'

##### Proposed Change/Modification:

- Add a separate principle addressing interoperability and open standards (both de facto and formal) within the "Insurance" section.

##### Notes:

- Interoperability and open standards are critical for ensuring that various scholarly systems and organizations can work together seamlessly. This addition would formalize the expectation that organizations maintain or work towards interoperability.
- This principle needs to recognize that “interoperability” can be a vague term so it's important to provide some clarity about what it means in the POSI context.
- This principle could also emphasize the importance of adhering to community standards (both de facto and formal) to ensure compatibility and integration with other open infrastructures.

> [!NOTE]
>
> It is encouraging to see that this proposal presents '"de facto" and "formal" standards on an equal footing.
>
> This is important because formal standards processes, particularly, can be dominated by commercial organizations to achieve regulatory lock-in and hinder the ability of smaller, non-profit organizations to provide infrastructure services. Commercial organizations have mastered the art of convening “[Bootleggers and Baptists](https://en.wikipedia.org/wiki/Bootleggers_and_Baptists),” into standards forums to create exceedingly complex standards that, while open, are challenging to implement without the expertise of numerous experts and developers. The accumulation of these standards and their integration into national procurement policies have made it extremely difficult for new entrants to compete in offering infrastructure services.
>
> I do wonder, though, if POSI really even needs to mention standards? I suspect that the primary concern that people have is that "open source" and "open data" can turn out to be unusable without documentation. And the typical formal standards process at least produces some documentation. But couldn't POSI address this by simply amending the existing language around "open source" and "open data" to read "open and documented source" and "open and documented data?"

#### Clarify “Open data (within constraints of privacy laws)” to include security as well as privacy laws.

##### Proposed Change/Modification:

- Clarify the principle to include security issues as well as privacy.
- Call for organizations to have a policy on what happens to private data in the case of a transfer to another organization.

> [!NOTE]
>
> POSI could use some explicit language on security, but it would be helpful if POSI provided examples of security concerns that might affect the ability of one to open data. The classic one is not wanting to reveal the migration patterns of endangered species.
>
> On the question of organizations having a policy on what happens to private data in the case of transfer to another organization, POSI should advocate two things:
>
> 1. A strong and unequivocal prohibition of the transfer of private data in case of transfer to another organization.
> 2. A call for POSI compliance to include that organizations should have a convenient and free mechanism for participants  to export their private information into documented formats so that they can easily move it if they want to.

#### Clarify “Available Data” to call for data to be deposited with a 3rd party

##### Proposed Change/Modification:

- Clarify the principle to include a call for data to be archived with a 3rd party.

##### Notes:

- Data should be deposited with a third party to ensure its availability if the original organization fails or its sustainability is in doubt.

> [!NOTE]
>
> This is a useful addition as long as it is not seen as a substitute for organizations making data broadly and openly available via periodic public data dumps.
>

### Other comments

The POSI principles have already been updated more often than most POSI adopters have updated their POSI self-audits.

Anybody who did not want to see POSI adoption expand could start seeding doubt in the community by saying “it isn’t really clear what you are signing up to when you adopt POSI because it keeps changing.”

I’ve also noted that the 1.1 revisions actually introduced some ambiguities in the text that this revision is now trying to fix.

In other words, updating POSI is a delicate balance. It has to be done, but it has to be done carefully.

I assume that POSI adopters intend the version numbers to convey real information about the extent of the editorial differences between versions, and that it is not just a marketing device to rekindle interest in POSI. If so, then I think that giving the proposed changes a “2.0” designation is misleading because it implies that [there are major new features](https://semver.org) in the proposed changes (which there are not)  - or worse- that it is not backward compatible with the previous versions of POSI (which isn't true either). This, of course, would feed into the narrative of anybody who wants to portray POSI as volatile and therefore difficult to endorse.

The call for comments on the proposed 2.0 revisions included the following "additional context around POSI”:

> - POSI is not an organization; POSI adopters are an informal group of those that have conducted self-assessments.

> - The POSI principles are not rules or a checklist; organizations or groups can adopt or interpret them to fit many different circumstances.

> - Our goal is for POSI self-assessments to be made publicly available and for interested communities to assess and monitor updates and progress.

This is  useful information.

I suspect that many people have more questions about the POSI adopters, the POSI site, and the expectations around the POSI audit process than they do about the text of the principles themselves.

And, unfortunately,  the POSI website has become less useful over the past two years. As of this writing (2025-03-05) , the single link to a feedback form has been broken for months, the dates on all the pages are clearly borked (Last modified on January 21, 212121), and, most worryingly, the list of POSI adopters and links to their self-audits is so out-of-date that it doesn’t even reflect that Crossref finally performed a third self-audit three months ago.

Frankly, if it were not for the comparatively recent call for comments on the proposed 2.0 revisions, it wouldn’t be clear if the POSI site was even still a going concern. That’s not a great way to increase adoption.

I suggest that, in addition to the proposed revision to the text of the principles, the POSI adopters also consider addressing some “meta” questions about the POSI’s presence, such as:

- Describing the purpose of the POSI site and how it is managed.

- Prominently highlighting the process of contacting existing POSI adopters for consultation.

- Clarifying the role of POSI adopters in the ongoing stewardship of the principles.

- Setting clear expectations regarding the frequency of the POSI self-audit process.

In any case, it’s good to see POSI adapt and refine the principles in order to remove rough edges and help new organizations adopt the principles without having to engage in a lengthy exegesis.



[^1]:   It appears that the enemies of alliteration and lighthearted self-deprecation have started to eschew the use of “POSI Posse” in favour of “POSI Adopters”
[^2]: Upton Sinclair’s “It is difficult to get a man to understand something when his salary depends upon his not understanding it!”