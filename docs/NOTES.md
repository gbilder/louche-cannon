
## Local Build

```
hugo serve -D
hugo
hugo new posts/new.md

# prettier
npx prettier content/posts/*.md --write
```

Check links:

https://github.com/filiph/linkcheck

`linkcheck :1313`


## TODO

- [x] Add JSON feed
- [x] Use theme as submodule
- [x] Switch to YAML config
- [x] Add DOIs to pages via Rogue Scholar
- [x] Switch to page bundles
- [x] Add JSON-feed
- [x] Create cite-me shortcode
- [ ] Modularise config
- [ ] Add marginal call-out
- [ ] Consider moving to Blowfish theme?

## Credits

[Configuring ATOM for paperMod](https://gist.github.com/lpar/7ded35d8f52fef7490a5be92e6cd6937)

[ATOM template for PaperMod](https://github.com/alswl/blog.alswl.com/tree/master/layouts)


## Example JSON-feed:

https://github.com/ropensci/roweb3/blob/main/themes/ropensci/layouts/blog/list.json.json

https://foosel.net/til/how-to-add-json-feed-support-to-hugo/


## Example of adding DOI:

https://github.com/ropensci/roweb3/blob/main/themes/ropensci/layouts/partials/blogs/doi.html

https://github.com/ropensci/roweb3/blob/main/themes/ropensci/layouts/partials/blogs/blog-single.html




https://citrinefox.com/posts/2023-10-03-hugo-rss/





###  Let's build a JSON-feed

This is the only json-schema I can find for the 1.1 version

https://github.com/sonicdoe/jsonfeed-schema/blob/master/schema-v1.1.json


## Next

- verify cite-me template works
- create documentaiton of JSON-FEED templates
- clean-up example documents
- verify that setting are back to production

Write me a Hugo archetype template that works like this:

When I issue the following command line:

```bash
hugo new content posts/2025-01-25-the-meaning-of-foo.md
```

It generates a file at `content/posts/2025-01-25-the-meaning-of-foo.md` with the following frontmatter metadata prepolulated based on the file name:

```yaml
---
title: "The Meaning of Foo"
date: "2025-01-25"
draft: true
---
```

