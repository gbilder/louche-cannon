# Cite-Me Hugo Shortcode Documentation

The `cite-me` shortcode creates a citation box for blog posts that includes author information, DOIs, and ORCID IDs.

## Usage

Add the shortcode to your markdown content:

```
{{< cite-me >}}
```

## Page Configuration

The shortcode reads citation information from the page's front matter. Configure your page with:

```yaml
title: "Your Post Title"
date: 2024-01-19
params:
  doi: "https://doi.org/10.xxxxx/xxxxx"
  authors:
    - name: "Author Name"
      orcid: "https://orcid.org/xxxx-xxxx-xxxx-xxxx"
    - name: "Second Author"
      orcid: "https://orcid.org/yyyy-yyyy-yyyy-yyyy"
```

## Features

- Supports multiple authors with their respective ORCID IDs
- Clickable DOI link
- Copy citation button
- Responsive design
- Semantic HTML structure

## Template Structure

The shortcode generates a citation box with:
- A header containing the "Cite as:" label and copy button
- Author list with ORCID IDs
- Publication date
- Title in italics
- DOI link

## Dependencies

- Requires the ORCID ID icon at `/ORCID-iD_icon_vector.svg`
- Copy icon SVG at `/icons/copy.svg`

## Styling

The template uses semantic class names that can be styled:
- `.citation`: The main container
- `.citation-header`: The header section
- `.copy-icon`: The copy button
- `.orcid-wrapper`: Container for ORCID information
- `.orcid-link`: ORCID ID link
- `.orcid-icon`: ORCID icon image
