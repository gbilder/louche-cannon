# Hugo JSON Feed Implementation Documentation

This document describes the implementation of JSON Feed v1.1 in Hugo templates.

## Overview

The JSON Feed implementation consists of several template files that work together to generate a valid JSON Feed:

- `list.jsonfeed.json` - Main template that generates the feed
- `jsonfeed-metadata.json` - Handles metadata rendering
- `jsonfeed-item.json` - Processes individual feed items
- `jsonfeed-author.json` - Manages author information

## Template Structure

### list.jsonfeed.json

The main template that:
1. Filters for regular pages with `syndicate = true`
2. Applies any RSS length limits
3. Builds required channel metadata (version, title, home_page_url)
4. Adds optional channel metadata (feed_url, language, description, etc)
5. Renders items array using jsonfeed-item.json

### jsonfeed-metadata.json

A utility template that:
- Takes a metadata dictionary as input
- Renders key-value pairs as JSON
- Handles proper JSON formatting and escaping
- Maintains consistent ordering of fields

### jsonfeed-item.json

Processes individual content items by:
1. Building required item metadata (title, id, url, dates)
2. Adding optional metadata like authors and content
3. Handling image URLs (banner_image, image)
4. Processing tags
5. Merging all metadata before rendering

### jsonfeed-author.json

Handles author information with support for:
1. Structured authors in page params
2. Author arrays in page params
3. Single authors in page params
4. Site-level authors
5. Fallback to "Unknown"

## Author Resolution Logic

The author template follows this precedence:

1. Check for structured authors in page params:
   ```yaml
   authors:
     - name: "Author Name"
       orcid: "https://orcid.org/..."
       avatar: "/images/avatar.jpg"
   ```

2. Check for author array in page params:
   ```yaml
   author: ["Author 1", "Author 2"]
   ```

3. Check for single author in page params:
   ```yaml
   author: "Author Name"
   ```

4. Fall back to site-level authors
5. Use "Unknown" if no author found

## Usage

To enable JSON Feed for a page:

1. Set `syndicate = true` in the page front matter
2. Optionally configure feed settings in site config:
   ```yaml
   services:
     rss:
       limit: 20  # Number of items to include
   ```

3. Add author information using any supported format
4. Configure site-level feed metadata in config:
   ```yaml
   params:
     description: "Feed description"
     icon: "/icon.png"
     favicon: "/favicon.ico"
     avatar: "/avatar.png"
   ```

## Feed URL

The JSON Feed will be available at: `/feed.json`

## Customization

To customize the feed:

1. Override any partial template in your site's layouts/partials/
2. Modify feed settings in site config
3. Add custom metadata in page front matter

## Implementation Notes

- Uses Hugo's built-in JSON encoding
- Handles proper escaping of HTML content
- Supports full text or summary content based on site config
- Processes relative URLs to absolute
- Maintains compatibility with JSON Feed v1.1 spec
